
Flutter Everywhere!

This Flutter projects acts as a boilerplate to create true universal apps from a single codebase.

Currently, the Flutter frameworks supports these platforms:

    Android
    iOS
    Windows
    macOS
    GNU+Linux
    Web (Chrome only)

Getting started

In order to build & deploy apps to specific, just run:

$ flutter build android|ios|windows|macos|linux|web

If want you really want is to test the app, first run flutter devices to check connected devices. Then, you'll have to run:

$ flutter run -d DEVICE

Download & install

First, clone the repository with the 'clone' command, or just download the zip.

$ git clone git@gitlab.com:tarripewe01/test_maia_raya.git

Then, download either Android Studio or Visual Studio Code, with their respective Flutter editor plugins. For more information about Flutter installation procedure, check the official install guide.

Install dependencies from pubspec.yaml by running flutter packages get from the project root (see using packages documentation for details and how to do this in the editor).

There you go, you can now open & edit the project. Enjoy!
Built with

    Flutter - Beautiful native apps in record time.
    Android Studio - Tools for building apps on every type of Android device.
    Visual Studio Code - Code editing. Redefined.

Authors : Tarri Peritha Westi
