import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_maia_raya/model/user_model.dart';
import 'package:test_maia_raya/services/auth_service.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthInitial());

  void register({
    required String name,
    required String email,
    required String password,
    required int telepon,
  }) async {
    try {
      emit(AuthLoading());

      UserModel user = await AuthService().register(
        email: email,
        password: password,
        name: name,
        telepon: telepon,
      );

      emit(AuthSuccess(user));
    } catch (e) {
      emit(AuthFailed(e.toString()));
    }
  }
}
