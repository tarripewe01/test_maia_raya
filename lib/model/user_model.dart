import 'package:equatable/equatable.dart';

class UserModel extends Equatable {
  final String id;
  final String name;
  final String email;
  final String password;
  final int telepon;

  UserModel({
    required this.id,
    required this.name,
    required this.email,
    required this.password,
    required this.telepon,
  });
  @override
  List<Object?> get props => [
        id,
        name,
        email,
        password,
        telepon,
      ];
}
