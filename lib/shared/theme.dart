import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

double defaultMargin = 25.0;
double defaultRadius = 10.0;

Color kPrimaryColor = Color(0xff2062ae);
Color kBaseColor = Color(0xfff5f6fa);
Color kWhiteColor = Color(0xffffffff);
Color kOrangeColor = Color(0xffca8568);
Color kGreyColor = Color(0xff8e8e8e);
Color kRedColor = Color(0xff0000);
Color kTransparentColor = Colors.transparent;

TextStyle primaryTextStyle = GoogleFonts.poppins(
  color: kPrimaryColor,
);

TextStyle greyTextStyle = GoogleFonts.poppins(
  color: kGreyColor,
);

TextStyle orangeTextStyle = GoogleFonts.poppins(
  color: kOrangeColor,
);

TextStyle whiteTextStyle = GoogleFonts.poppins(
  color: kBaseColor,
);

FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extraBold = FontWeight.w800;
FontWeight black = FontWeight.w900;
