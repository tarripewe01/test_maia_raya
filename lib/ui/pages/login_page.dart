import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:test_maia_raya/services/local_auth.dart';
import '../../shared/theme.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isAvailable = false;
  bool isAuthenticated = false;
  String text = "Please Check Biometric Availability";
  LocalAuthentication localAuthentication = LocalAuthentication();

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    Widget Logo() {
      return Container(
        width: 150,
        margin: EdgeInsets.only(top: 10, left: 0),
        child: Image.asset(
          'assets/logo.png',
          // height: 150,
          // width: 200,
        ),
      );
    }

    Widget inputSection() {
      Widget lupaPassword() {
        return Container(
          width: double.infinity,
          margin: EdgeInsets.only(
            bottom: 15,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              InkWell(
                onTap: () {},
                child: Text(
                  'Lupa Password?',
                  style: TextStyle(
                    color: kOrangeColor,
                  ),
                ),
              ),
            ],
          ),
        );
      }

      Widget emailInput() {
        return Container(
          margin: EdgeInsets.only(bottom: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Email',
                style: TextStyle(color: kOrangeColor),
              ),
              SizedBox(
                height: 6,
              ),
              TextFormField(
                controller: _emailController,
                cursorColor: kOrangeColor,
                decoration: InputDecoration(
                  hintText: 'Email Anda',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      defaultRadius,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      defaultRadius,
                    ),
                    borderSide: BorderSide(
                      color: kOrangeColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }

      Widget passwordInput() {
        return Container(
          margin: EdgeInsets.only(bottom: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Password',
                style: TextStyle(color: kOrangeColor),
              ),
              SizedBox(
                height: 6,
              ),
              TextFormField(
                controller: _passwordController,
                cursorColor: kOrangeColor,
                obscureText: true,
                decoration: InputDecoration(
                  hintText: 'Password Anda',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      defaultRadius,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      defaultRadius,
                    ),
                    borderSide: BorderSide(
                      color: kOrangeColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }

      Widget masuk() {
        return Container(
          margin: EdgeInsets.only(
            bottom: 5,
          ),
          child: Column(
            children: [
              Text(
                'Masuk',
                style: TextStyle(
                    color: kPrimaryColor,
                    fontWeight: bold,
                    fontSize: 42,
                    letterSpacing: 1.5),
              ),
            ],
          ),
        );
      }

      Widget masukButton() {
        return Container(
          width: double.infinity,
          height: 55,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Container(
                  width: 280,
                  child: TextButton(
                    onPressed: () async {
                      await _firebaseAuth
                          .signInWithEmailAndPassword(
                            email: _emailController.text,
                            password: _passwordController.text,
                          )
                          .then(
                            (value) => Navigator.pushNamed(context, '/home'),
                          );
                    },
                    style: TextButton.styleFrom(
                      backgroundColor: kPrimaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(
                          defaultRadius,
                        ),
                      ),
                    ),
                    child: Text(
                      'Masuk',
                      style: whiteTextStyle.copyWith(
                        fontSize: 18,
                        fontWeight: bold,
                      ),
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: () async {
                  final isAuthenticated = await LocalAuth.authenticate();

                  if (isAuthenticated) {
                    Navigator.pushNamed(context, '/home');
                  }
                },
                child: Container(
                  margin: EdgeInsets.only(left: 15),
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/fingerprint.png'),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }

      Widget tacButton() {
        return Container(
          margin: EdgeInsets.only(
            top: 15,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('belum punya akun?'),
              Container(
                padding: EdgeInsets.only(
                  left: 5,
                ),
                child: InkWell(
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      '/register',
                    );
                  },
                  child: Text(
                    'daftar disini',
                    style: TextStyle(
                      color: kOrangeColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }

      return Container(
        margin: EdgeInsets.only(top: 30),
        padding: EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 30,
        ),
        decoration: BoxDecoration(
          // color: kWhiteColor,
          borderRadius: BorderRadius.circular(
            defaultRadius,
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Logo(),
            masuk(),
            emailInput(),
            passwordInput(),
            lupaPassword(),
            masukButton(),
            tacButton(),
          ],
        ),
      );
    }

    return Scaffold(
      // backgroundColor: kBaseColor,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 10),
          padding: EdgeInsets.symmetric(
            horizontal: defaultMargin,
          ),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // Logo(),
              inputSection(),
            ],
          ),
        ),
      ),
    );
  }
}
