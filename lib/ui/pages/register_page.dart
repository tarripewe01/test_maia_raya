import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_maia_raya/cubit/auth_cubit/auth_cubit.dart';
import '../../shared/theme.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _teleponController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    Widget Logo() {
      return Container(
        width: 150,
        margin: EdgeInsets.only(top: 10, left: 0),
        child: Image.asset(
          'assets/logo.png',
          // height: 150,
          // width: 200,
        ),
      );
    }

    Widget inputSection() {
      Widget nameInput() {
        return Container(
          margin: EdgeInsets.only(bottom: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Nama',
                style: TextStyle(color: kOrangeColor),
              ),
              SizedBox(
                height: 6,
              ),
              TextFormField(
                key: ValueKey('name'),
                controller: _nameController,
                cursorColor: kOrangeColor,
                decoration: InputDecoration(
                  hintText: 'Nama Anda',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      defaultRadius,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      defaultRadius,
                    ),
                    borderSide: BorderSide(
                      color: kOrangeColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }

      Widget emailInput() {
        return Container(
          margin: EdgeInsets.only(bottom: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Email',
                style: TextStyle(color: kOrangeColor),
              ),
              SizedBox(
                height: 6,
              ),
              TextFormField(
                key: ValueKey('email'),
                controller: _emailController,
                cursorColor: kOrangeColor,
                validator: (value) {
                  if (value!.isEmpty || !value.contains('@')) {
                    return 'Masukkan email yang benar';
                  }
                },
                decoration: InputDecoration(
                  hintText: 'Email Anda',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      defaultRadius,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      defaultRadius,
                    ),
                    borderSide: BorderSide(
                      color: kOrangeColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }

      Widget passwordInput() {
        return Container(
          margin: EdgeInsets.only(bottom: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Password',
                style: TextStyle(color: kOrangeColor),
              ),
              SizedBox(
                height: 6,
              ),
              TextFormField(
                key: ValueKey('password'),
                controller: _passwordController,
                cursorColor: kOrangeColor,
                obscureText: true,
                decoration: InputDecoration(
                  hintText: 'Password Anda',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      defaultRadius,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      defaultRadius,
                    ),
                    borderSide: BorderSide(
                      color: kOrangeColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }

      Widget teleponInput() {
        return Container(
          margin: EdgeInsets.only(bottom: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Nomor Telp',
                style: TextStyle(color: kOrangeColor),
              ),
              SizedBox(
                height: 6,
              ),
              TextFormField(
                key: ValueKey('No Telepon'),
                controller: _teleponController,
                cursorColor: kOrangeColor,
                decoration: InputDecoration(
                  hintText: 'Nomor Telepon Anda',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      defaultRadius,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      defaultRadius,
                    ),
                    borderSide: BorderSide(
                      color: kOrangeColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }

      Widget daftar() {
        return Container(
          margin: EdgeInsets.only(
            bottom: 5,
          ),
          child: Column(
            children: [
              Text(
                'Daftar',
                style: TextStyle(
                    color: kPrimaryColor,
                    fontWeight: bold,
                    fontSize: 42,
                    letterSpacing: 1.5),
              ),
            ],
          ),
        );
      }

      Widget daftarButton() {
        return BlocConsumer<AuthCubit, AuthState>(
          listener: (context, state) {
            if (state is AuthSuccess) {
              Navigator.pushNamedAndRemoveUntil(
                  context, '/login', (route) => false);
            } else if (state is AuthFailed) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: kPrimaryColor,
                  content: Text(
                    state.error,
                    style: TextStyle(
                      color: kWhiteColor,
                    ),
                  ),
                ),
              );
            }
          },
          builder: (context, state) {
            if (state is AuthLoading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            return Container(
              width: double.infinity,
              height: 55,
              child: TextButton(
                onPressed: () {
                  context.read<AuthCubit>().register(
                        name: _nameController.text,
                        email: _emailController.text,
                        password: _passwordController.text,
                        telepon: int.parse(_teleponController.text),
                      );
                },
                style: TextButton.styleFrom(
                  backgroundColor: kPrimaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                      defaultRadius,
                    ),
                  ),
                ),
                child: Text(
                  'Daftar',
                  style: whiteTextStyle.copyWith(
                    fontSize: 18,
                    fontWeight: bold,
                  ),
                ),
              ),
            );
          },
        );
      }

      Widget tacButton() {
        return Container(
          margin: EdgeInsets.only(
            top: 15,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('sudah punya akun?'),
              Container(
                padding: EdgeInsets.only(
                  left: 5,
                ),
                child: InkWell(
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      '/login',
                    );
                  },
                  child: Text(
                    'masuk disini',
                    style: TextStyle(
                      color: kOrangeColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }

      return Container(
        margin: EdgeInsets.only(top: 30),
        padding: EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 30,
        ),
        decoration: BoxDecoration(
          // color: kWhiteColor,
          borderRadius: BorderRadius.circular(
            defaultRadius,
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Logo(),
            daftar(),
            nameInput(),
            emailInput(),
            passwordInput(),
            teleponInput(),
            daftarButton(),
            tacButton(),
          ],
        ),
      );
    }

    return Scaffold(
      // backgroundColor: kBaseColor,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 10),
          padding: EdgeInsets.symmetric(
            horizontal: defaultMargin,
          ),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // Logo(),
              inputSection(),
            ],
          ),
        ),
      ),
    );
  }
}
