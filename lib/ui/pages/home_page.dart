import 'package:flutter/material.dart';
import 'package:test_maia_raya/shared/theme.dart';
import 'package:test_maia_raya/ui/pages/pocket_page.dart';
import 'package:test_maia_raya/ui/pages/scan_page.dart';
import 'package:test_maia_raya/ui/widgets/custom_bottom_bar.dart';

import 'account_page.dart';
import 'more_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({
    Key? key,
  }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = HomePage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBaseColor,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Container(
                child: Stack(
                  children: [
                    BackgoundColor(),
                    Content(),
                  ],
                ),
              ),
              // MenuUtama(),
            ],
          ),
        ),
      ),
      floatingActionButton: Container(
        height: 80,
        width: 80,
        margin: EdgeInsets.only(
          top: 30,
        ),
        child: FloatingActionButton(
          backgroundColor: kWhiteColor,
          onPressed: () {},
          child: Icon(
            Icons.qr_code_scanner,
            size: 55,
            color: kOrangeColor,
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 80,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconBottomBar(
                  icon: Icons.home,
                  isSelected: true,
                ),
                IconBottomBar(icon: Icons.account_balance_wallet),
                SizedBox.shrink(),
                IconBottomBar(
                  icon: Icons.account_circle,
                ),
                IconBottomBar(
                  icon: Icons.more_horiz,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Content extends StatelessWidget {
  const Content({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: 100,
                  width: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        'assets/logo_putih.png',
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(right: 35),
                  child: Icon(
                    Icons.notifications,
                    color: kWhiteColor,
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 35),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Selamat Datang,',
                    style: TextStyle(
                      color: kWhiteColor,
                      fontSize: 12,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Tarri Pewe',
                    style: TextStyle(
                      color: kWhiteColor,
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Saldo :',
                    style: TextStyle(
                      color: kWhiteColor,
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Rp 87.422.500',
                          style: TextStyle(
                            color: kWhiteColor,
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Icon(
                          Icons.remove_red_eye,
                          color: kWhiteColor,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: kWhiteColor,
              borderRadius: BorderRadius.circular(18),
            ),
            width: 300,
            height: 90,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Icon(
                      Icons.trending_up,
                      color: kPrimaryColor,
                      size: 30,
                    ),
                    Text(
                      'Suku Bunga',
                      style: TextStyle(
                        color: kPrimaryColor,
                        fontSize: 10,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Icon(
                      Icons.attach_money,
                      color: kPrimaryColor,
                      size: 30,
                    ),
                    Text(
                      'Suku Bunga',
                      style: TextStyle(
                        color: kPrimaryColor,
                        fontSize: 10,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Icon(
                      Icons.history,
                      color: kPrimaryColor,
                      size: 30,
                    ),
                    Text(
                      'Suku Bunga',
                      style: TextStyle(
                        color: kPrimaryColor,
                        fontSize: 10,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class BackgoundColor extends StatelessWidget {
  const BackgoundColor({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kPrimaryColor,
      height: 300,
      width: double.infinity,
    );
  }
}
