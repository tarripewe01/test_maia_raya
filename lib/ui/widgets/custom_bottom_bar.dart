import 'package:flutter/material.dart';
import 'package:test_maia_raya/shared/theme.dart';

class IconBottomBar extends StatelessWidget {
  final bool isSelected;
  final icon;
  const IconBottomBar({
    Key? key,
    this.isSelected = false,
    this.icon,
  }) : super(key: key);

  Widget build(BuildContext context) {
    return Icon(
      icon,
      size: 40,
      color: isSelected ? kPrimaryColor : kGreyColor,
    );
  }
}
