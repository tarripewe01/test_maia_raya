import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:test_maia_raya/model/user_model.dart';

class UserServices {
  CollectionReference _userReferences =
      FirebaseFirestore.instance.collection('users');

  Future<void> setUser(UserModel user) async {
    try {
      _userReferences.doc(user.id).set(
        {
          'name': user.name,
          'email': user.email,
          'password': user.password,
          'telepon': user.telepon,
        },
      );
    } catch (e) {
      throw e;
    }
  }
}
