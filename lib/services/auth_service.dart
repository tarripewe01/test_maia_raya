import 'package:firebase_auth/firebase_auth.dart';
import 'package:test_maia_raya/model/user_model.dart';
import 'package:test_maia_raya/services/user_service.dart';

class AuthService {
  FirebaseAuth _auth = FirebaseAuth.instance;

  Future<UserModel> register({
    required String email,
    required String password,
    required String name,
    required int telepon,
  }) async {
    try {
      UserCredential userCredential =
          await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      UserModel user = UserModel(
        id: userCredential.user!.uid,
        name: name,
        email: email,
        password: password,
        telepon: telepon,
      );

      await UserServices().setUser(user);

      return user;
    } catch (e) {
      throw e;
    }
  }
}
